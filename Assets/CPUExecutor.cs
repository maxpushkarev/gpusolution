﻿using UnityEngine;
using UnityEditor;
using System.IO;

public class CPUExecutor : MonoBehaviour {

	private const int THREADS_PER_GROUP = 256;
	private const string EXECUTOR_KERNEL = "executor";
	private const string FINALIZER_KERNEL = "finalizer";
	private const string INPUT_DATA_NAME = "inputData";
	private const string CURRENT_STATE_NAME = "currentState";
	private const string NEXT_STATE_NAME = "nextState";
	private const string TEMPLATE_A_NAME = "A";
	private const string TEMPLATE_B_NAME = "B";

	[SerializeField]
	private float discreteTimeStep = 1.0f;
	[SerializeField]
	private int iterationCount = 1;
	[SerializeField]
	private Texture2D originalImage;
	[SerializeField]
	private float[] templateA;
	[SerializeField]
	private float[] templateB;
	[SerializeField]
	private float templateI;
	[SerializeField]
	private ComputeShader gpuExecutor;

	private int gpuExecutorKernel;
	private int gpuFinalizerKernel;

	private void Start(){
		
		gpuExecutorKernel = gpuExecutor.FindKernel (EXECUTOR_KERNEL);
		gpuFinalizerKernel = gpuExecutor.FindKernel (FINALIZER_KERNEL);
		Color[] colors = originalImage.GetPixels ();
		int colorsLength = colors.Length;
		int size = Mathf.FloorToInt(Mathf.Sqrt (colorsLength));

		gpuExecutor.SetFloat ("DISCRETE_TIME_STEP", discreteTimeStep);
		gpuExecutor.SetFloat ("I", templateI);
		gpuExecutor.SetInt ("SIZE", size);

		using (ComputeBuffer inputGPUData = new ComputeBuffer (colorsLength, 16), 
			currentGPUState = new ComputeBuffer(colorsLength, 16), 
			nextGPUState = new ComputeBuffer(colorsLength, 16),
			templateABuffer = new ComputeBuffer(templateA.Length, 4),
			templateBBuffer = new ComputeBuffer(templateB.Length, 4)
		) {

			gpuExecutor.SetBuffer (gpuExecutorKernel, TEMPLATE_A_NAME, templateABuffer);
			gpuExecutor.SetBuffer (gpuExecutorKernel, TEMPLATE_B_NAME, templateBBuffer);
			gpuExecutor.SetBuffer (gpuExecutorKernel, INPUT_DATA_NAME, inputGPUData);
			gpuExecutor.SetBuffer (gpuFinalizerKernel, INPUT_DATA_NAME, inputGPUData);

			inputGPUData.SetData (colors);
			templateABuffer.SetData (templateA);
			templateBBuffer.SetData (templateB);

			int iterationNumber = 0;
			while (iterationNumber < iterationCount) {

				if (iterationNumber % 2 == 0) {
					gpuExecutor.SetBuffer (gpuExecutorKernel, CURRENT_STATE_NAME, currentGPUState);
					gpuExecutor.SetBuffer (gpuExecutorKernel, NEXT_STATE_NAME, nextGPUState);
				} else {
					gpuExecutor.SetBuffer (gpuExecutorKernel, NEXT_STATE_NAME, currentGPUState);
					gpuExecutor.SetBuffer (gpuExecutorKernel, CURRENT_STATE_NAME, nextGPUState);
				}

				gpuExecutor.Dispatch (gpuExecutorKernel, colorsLength / THREADS_PER_GROUP, 1, 1);
				iterationNumber++;
			}
				

			gpuExecutor.SetBuffer (gpuFinalizerKernel, NEXT_STATE_NAME, nextGPUState);
			gpuExecutor.Dispatch (gpuFinalizerKernel, colorsLength / THREADS_PER_GROUP, 1, 1);

			Color[] result = new Color[colorsLength];
			if (iterationNumber % 2 == 0) {
				nextGPUState.GetData (result);
			} else {
				currentGPUState.GetData (result);
			}
			Texture2D resultTexture = new Texture2D (size, size);
			resultTexture.SetPixels (result);
			resultTexture.Apply ();

			byte[] bytes = resultTexture.EncodeToPNG();
			Object.Destroy(resultTexture);
			string path = Application.dataPath + "/output_cat.png";
			string assetPath = "Assets/output_cat.png";
			File.WriteAllBytes(path, bytes);
			AssetDatabase.Refresh ();
			TextureImporter textureImporter = TextureImporter.GetAtPath (assetPath) as TextureImporter;
			textureImporter.mipmapEnabled = false;
			textureImporter.alphaIsTransparency = true;
			AssetDatabase.ImportAsset(assetPath, ImportAssetOptions.ForceUpdate);

		}
	}
}