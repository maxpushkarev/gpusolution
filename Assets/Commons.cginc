#define THREADS_PER_GROUP 256

float DISCRETE_TIME_STEP, I;
int SIZE;

RWStructuredBuffer<float4> inputData;
RWStructuredBuffer<float4> currentState;
RWStructuredBuffer<float4> nextState;

RWStructuredBuffer<float> A;
RWStructuredBuffer<float> B;

void execute(uint index) {
	float4 state = 0;
	uint row = index / SIZE;
	uint column = index % SIZE;

	for(uint i = 0; i<25; i++) {
		int newRow = row + (i / 5) - 1;
		int newColumn = column + (i % 5) - 1;
		int linearIndex = newRow * SIZE + column;
		float4 possibleNeighboor = currentState[linearIndex];
		float4 empty = float4(0.0f, 0.0f, 0.0f, 0.0f);
		bool withinRange = (newRow < SIZE) && (newRow >= 0) && (newColumn < SIZE) && (newColumn >= 0);
		float4 stateAggr = lerp(empty, possibleNeighboor, withinRange);
		float4 inputAggr = lerp(empty, inputData[linearIndex], withinRange);

		state += clamp(stateAggr, -1.0f, 1.0f) * A[i];
		state += inputAggr * B[i];
	}

	state *= DISCRETE_TIME_STEP;
	state += I;
	state += currentState[index];
	//state -= currentState[index] * DISCRETE_TIME_STEP;

	nextState[index] = state;
}
